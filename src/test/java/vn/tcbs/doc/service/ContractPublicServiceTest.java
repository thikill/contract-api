package vn.tcbs.doc.service;

import vn.tcbs.doc.ContractApiApplication;
import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.repository.ContractRepository;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.service.dto.ContractTraceDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ContractApiApplication.class)
@Transactional
public class ContractPublicServiceTest {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractPublicService contractPublicService;

    @Test
    public void testFindAll01() {
    	List<ContractDetailsDTO> details = contractPublicService.findAll(95);
    	assert(details.size()==2);
    }
    
    @Test
    public void testFindAll02() {
/*    	List<ContractDetailsDTO> details = contractPublicService.findAll("TCBond-TCBond082017");
    	assert(details.size()==2);    	*/
    }
    
    @Test
    public void testFindAll03() {
    	Calendar cal =Calendar.getInstance();
    	cal.set(2018, 4, 1);
    	List<ContractDetailsDTO> details = contractPublicService.findAll(95,new Date(cal.getTimeInMillis()));
    	assert(details.size()==2);    	
    }    
    
    @Test
    public void testFindAll04() {
    	Calendar cal =Calendar.getInstance();
    	cal.set(2018, 4, 1);
    	List<ContractDetailsDTO> details = contractPublicService.findAll("TCBond-TCBond082017",new Date(cal.getTimeInMillis()));
    	assert(details.size()==2);    	
    }       
    
    @Test
    public void testSign() {
    	Calendar cal =Calendar.getInstance();
    	cal.set(2018, 4, 1);
    	//ContractTraceDTO contractTrace =contractPublicService.sign(1, 1, "ThiMB"); 
    	//assert(contractTrace!= null);    	
    }       
    

 
}
