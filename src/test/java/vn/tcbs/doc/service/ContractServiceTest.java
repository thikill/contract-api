package vn.tcbs.doc.service;

import vn.tcbs.doc.ContractApiApplication;
import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.repository.ContractRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertNull;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ContractApiApplication.class)
@Transactional
public class ContractServiceTest {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractService contractService;

    @Test
    public void assertThatUserMustExistToResetPassword() {
    	List<Contract> contracts = contractRepository.findAll();
    	System.out.println(contracts.size());
    	assertThat(contracts.size()>0);
    }
    
    
    @Test
    public void assertFindAllActive() {
    	List<Contract> contracts = contractRepository.findByAcive();
    	System.out.println(contracts.size());
    	//assertNull(contracts.get(0).getContract());
    }

 
}
