package vn.tcbs.doc.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.tcbs.doc.domain.ContractTrace;
import vn.tcbs.doc.repository.ContractTraceRepository;
import vn.tcbs.doc.service.ContractTraceService;
import vn.tcbs.doc.service.dto.ContractTraceDTO;
import vn.tcbs.doc.service.mapper.ContractTraceMapper;

/**
 * Service Implementation for managing ContractTrace.
 */
@Service
@Transactional
public class ContractTraceServiceImpl implements ContractTraceService{

    private final Logger log = LoggerFactory.getLogger(ContractTraceServiceImpl.class);
    
    private final ContractTraceRepository contractTraceRepository;

    private final ContractTraceMapper contractTraceMapper;

    public ContractTraceServiceImpl(ContractTraceRepository contractTraceRepository, ContractTraceMapper contractTraceMapper) {
        this.contractTraceRepository = contractTraceRepository;
        this.contractTraceMapper = contractTraceMapper;
    }

    /**
     * Save a contract_trace.
     *
     * @param contractTraceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ContractTraceDTO save(ContractTraceDTO contractTraceDTO) {
        log.debug("Request to save ContractTrace : {}", contractTraceDTO);
        ContractTrace contractTrace = contractTraceMapper.toEntity(contractTraceDTO);
        contractTrace = contractTraceRepository.save(contractTrace);
        ContractTraceDTO result = contractTraceMapper.toDto(contractTrace);
        return result;
    }

    /**
     *  Get all the contract_traces.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ContractTraceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contract_traces");
        Page<ContractTrace> result = contractTraceRepository.findAll(pageable);
        return result.map(contract_trace -> contractTraceMapper.toDto(contract_trace));
    }

    /**
     *  Get one contract_trace by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ContractTraceDTO findOne(Long id) {
        log.debug("Request to get ContractTrace : {}", id);
        ContractTrace contractTrace = contractTraceRepository.findOne(id);
        ContractTraceDTO contractTraceDTO = contractTraceMapper.toDto(contractTrace);
        return contractTraceDTO;
    }

    /**
     *  Delete the  contract_trace by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ContractTrace : {}", id);
        contractTraceRepository.delete(id);
    }
}
