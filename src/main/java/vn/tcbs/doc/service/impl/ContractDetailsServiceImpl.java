package vn.tcbs.doc.service.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.repository.ContractDetailsRepository;
import vn.tcbs.doc.repository.ContractRepository;
import vn.tcbs.doc.service.ContractDetailsService;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.service.mapper.ContractDetailsMapper;

/**
 * Service Implementation for managing ContractDetails.
 */
@Service
@Transactional
public class ContractDetailsServiceImpl implements ContractDetailsService {

	private final Logger log = LoggerFactory.getLogger(ContractDetailsServiceImpl.class);

	private final ContractDetailsRepository contractDetailsRepository;
	private final ContractDetailsMapper contractDetailsMapper;
	private final ContractRepository contractRepository;

	public ContractDetailsServiceImpl(ContractDetailsRepository contractDetailsRepository,
			ContractRepository contractRepository, ContractDetailsMapper contractDetailsMapper) {
		this.contractDetailsRepository = contractDetailsRepository;
		this.contractRepository = contractRepository;
		this.contractDetailsMapper = contractDetailsMapper;
	}

	/**
	 * Save a contract_details.
	 *
	 * @param contractDetailsDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public ContractDetailsDTO save(ContractDetailsDTO contractDetailsDTO) {
		log.debug("Request to save ContractDetails : {}", contractDetailsDTO);
		ContractDetails contractDetails = contractDetailsMapper.toEntity(contractDetailsDTO);

		if (contractDetails.getId() == null) {
			contractDetails.created_date(new Date());
			contractDetails.setUpdated_time(new Date());
		} else {
			contractDetails.setUpdated_time(new Date());
		}
		contractDetails = contractDetailsRepository.save(contractDetails);
		ContractDetailsDTO result = contractDetailsMapper.toDto(contractDetails);
		return result;
	}

	/**
	 * Get all the contract_details.
	 * 
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public List<ContractDetailsDTO> findAll(Long contractId) {
		log.debug("Request to get all ContractDetails");
		Contract contract = contractRepository.findOne(contractId);

		List<ContractDetailsDTO> result = contract.getContract_details().stream().map(contractDetailsMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	/**
	 * Get one contract_details by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public ContractDetailsDTO findOne(Long id) {
		log.debug("Request to get ContractDetails : {}", id);
		ContractDetails contractDetails = contractDetailsRepository.findOne(id);
		ContractDetailsDTO contractDetailsDTO = contractDetailsMapper.toDto(contractDetails);
		return contractDetailsDTO;
	}

	/**
	 * Delete the contract_details by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete ContractDetails : {}", id);
		contractDetailsRepository.delete(id);
	}
}
