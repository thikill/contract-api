package vn.tcbs.doc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import vn.tcbs.doc.domain.InvInvestmentProduct;
import vn.tcbs.doc.repository.InvestProductRepository;
import vn.tcbs.doc.service.InvProductService;
@Service
public class InvProductServiceImpl implements InvProductService {
    private final Logger log = LoggerFactory.getLogger(InvProductServiceImpl.class);
    private final InvestProductRepository investProductRepository;


    public InvProductServiceImpl(InvestProductRepository investProductRepository) {
        this.investProductRepository = investProductRepository;
    }
	@Override
	public List<InvInvestmentProduct> findProduct() {
		return investProductRepository.findProduct();
	}

}
