package vn.tcbs.doc.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.domain.ContractTrace;
import vn.tcbs.doc.repository.ContractDetailsRepository;
import vn.tcbs.doc.repository.ContractRepository;
import vn.tcbs.doc.repository.ContractTraceRepository;
import vn.tcbs.doc.repository.VelocityRepository;
import vn.tcbs.doc.service.ContractPublicService;
import vn.tcbs.doc.service.dto.ContractDetailUnsignDTO;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.service.dto.ContractTraceDTO;
import vn.tcbs.doc.service.mapper.ContractDetailsMapper;
import vn.tcbs.doc.service.mapper.ContractTraceMapper;

@Service
@Transactional
public class ContractPublicServiceImpl implements ContractPublicService {
	private final Logger log = LoggerFactory.getLogger(ContractPublicServiceImpl.class);

	private final ContractDetailsRepository contractDetailsRepository;
	private final ContractRepository contractRepository;
	private final ContractDetailsMapper contractDetailsMapper;
	private final ContractTraceMapper contractTraceMapper;
	private final ContractTraceRepository contractTraceRepository;
	private final VelocityRepository  velocityRepository;
	public VelocityEngine getVelocityEngine() {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();
		return ve;
	}


	public ContractPublicServiceImpl(ContractRepository contractRepository,
			ContractDetailsRepository contractDetailsRepository, ContractDetailsMapper contractDetailsMapper,
			ContractTraceRepository contractTraceRepository, ContractTraceMapper contractTraceMapper, VelocityRepository  velocityRepository ) {
		this.contractDetailsRepository = contractDetailsRepository;
		this.contractDetailsMapper = contractDetailsMapper;
		this.contractRepository = contractRepository;
		this.contractTraceRepository = contractTraceRepository;
		this.contractTraceMapper = contractTraceMapper;
		this.velocityRepository = velocityRepository;
		
	}

	@Override
	public List<ContractDetailsDTO> findAll(long contractID) {
		Contract contract = contractRepository.findOne(contractID);

		List<ContractDetailsDTO> result = contract.getContract_details().stream().map(contractDetailsMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public List<ContractDetailsDTO> findAll(int productID, Date applyDate) {
		Contract contract = contractRepository.findOne(productID, applyDate);

		List<ContractDetailsDTO> result = contract.getContract_details().stream().map(contractDetailsMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public List<ContractDetailsDTO> findAll(String productCode, Date applyDate) {
		Contract contract = contractRepository.findOne(productCode, applyDate);

		List<ContractDetailsDTO> result = contract.getContract_details().stream().map(contractDetailsMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public List<ContractTraceDTO> findAllSigned(int orderID) {
		List<ContractTrace> listContractTrace = contractRepository.findAllSigned(orderID);

		List<ContractTraceDTO> result = listContractTrace.stream().map(contractTraceMapper::toDto)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

	@Override
	public List<ContractDetailsDTO> findAllUnsign(ContractDetailUnsignDTO contractDetailUnsignDTO) {
		List<ContractDetailsDTO> result = null;
		try {
			int orderID = contractDetailUnsignDTO.getOrderID();
			int contractID = contractRepository.getContractIDByDetailID(orderID);
			Contract contract = contractRepository.findOne(new Integer(contractID).longValue());
			Set<ContractDetails> details = contract.getContract_details();
			List<ContractTrace> contractTraces = contractRepository.findAllSigned(orderID);

			List<ContractDetails> listUnsign = new ArrayList<>();
			for (ContractDetails contractDetails : details) {
				boolean unsign = false;
				for (ContractTrace contractTrace : contractTraces) {
					if (contractDetails.getId() == contractTrace.getContract_detail_id().longValue()) {
						unsign = true;
						break;
					}
				}
				if (unsign == false) {
					listUnsign.add(contractDetails);
				}
			}
			result = listUnsign.stream().map(contractDetailsMapper::toDto)
					.collect(Collectors.toCollection(LinkedList::new));
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		
		for (ContractDetailsDTO dto: result) {
			String text = velocityRepository.mergeMapToString(dto.getContent(), contractDetailUnsignDTO.getData());
			dto.setContent(text);
		}
		
		
				
		
		return result;
	}

	@Override
	public ContractTraceDTO sign(ContractTraceDTO contractTraceDTO) {
		log.debug("Request to save ContractTrace : {}", contractTraceDTO);
		ContractTrace contractTrace = contractTraceMapper.toEntity(contractTraceDTO);
		contractTrace.setSign_time(new Date());
		contractTrace = contractTraceRepository.save(contractTrace);
		ContractTraceDTO result = contractTraceMapper.toDto(contractTrace);
		return result;
	}

}
