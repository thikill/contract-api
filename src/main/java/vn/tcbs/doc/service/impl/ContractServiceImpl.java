package vn.tcbs.doc.service.impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.repository.ContractDetailsRepository;
import vn.tcbs.doc.repository.ContractRepository;
import vn.tcbs.doc.service.ContractService;
import vn.tcbs.doc.service.dto.ContractDTO;
import vn.tcbs.doc.service.mapper.ContractMapper;

/**
 * Service Implementation for managing Contract.
 */
@Service
@Transactional
public class ContractServiceImpl implements ContractService {

	private final Logger log = LoggerFactory.getLogger(ContractServiceImpl.class);

	private final ContractRepository contractRepository;
	private final ContractDetailsRepository contractDetailsRepository;
	private final ContractMapper contractMapper;

	public ContractServiceImpl(ContractRepository contractRepository,
			ContractDetailsRepository contractDetailsRepository, ContractMapper contractMapper) {
		this.contractRepository = contractRepository;
		this.contractDetailsRepository = contractDetailsRepository;
		this.contractMapper = contractMapper;
	}

	/**
	 * Save a contract.
	 *
	 * @param contractDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public ContractDTO save(ContractDTO contractDTO) {
		log.debug("Request to save Contract : {}", contractDTO);
		Contract contract = contractMapper.toEntity(contractDTO);
		Integer copyFromContractId = contractDTO.getCopy_contract_id();
		if (copyFromContractId != null) {
			// save new contracts
			Contract contractObj = contractRepository.findOne(copyFromContractId.longValue());
			Set<ContractDetails> details = contractObj.getContract_details();
			contract.setActive(1);
			contract.setApply_status(1);
			contract.setCreated_time(new Date());
			contract.setUpdated_time(new Date());
			contract = contractRepository.save(contract);
			ContractDTO result = contractMapper.toDto(contract);
			for (ContractDetails contractDetails : details) {
				ContractDetails detail = new ContractDetails();
				detail.setContent(contractDetails.getContent());
				detail.setItem_name(contractDetails.getItem_name());
				detail.setContract(fromId(contract.getId()));
				detail.created_date(new Date());
				detail.setUpdated_time(new Date());
				contractDetailsRepository.save(detail);
			}
			return result;
		}
		if (contract.getId() == null) {
			contract.setActive(1);
			contract.setApply_status(1);
			contract.setCreated_time(new Date());
			contract.setUpdated_time(new Date());
		} else {
			contract.setUpdated_time(new Date());
		}
		contract = contractRepository.save(contract);
		ContractDTO result = contractMapper.toDto(contract);
		return result;
	}

	private Contract fromId(Long id) {
		if (id == null) {
			return null;
		}
		Contract contract = new Contract();
		contract.setId(id);
		return contract;
	}

	/**
	 * Get all the contracts.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<ContractDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Contracts");
		// Page<Contract> result = contractRepository.findAll(pageable);
		Page<Contract> result = contractRepository.findByAcive(pageable);
		return result.map(contract -> contractMapper.toDto(contract));
	}

	/**
	 * Get one contract by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public ContractDTO findOne(Long id) {
		log.debug("Request to get Contract : {}", id);
		Contract contract = contractRepository.findOne(id);
		ContractDTO contractDTO = contractMapper.toDto(contract);
		return contractDTO;
	}

	/**
	 * Delete the contract by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Contract : {}", id);
		contractRepository.delete(id);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return contractRepository.countActive() / 20 + 1;
	}

	@Override
	public List<ContractDTO> findAll() {
		List<Contract> contracts = contractRepository.findByAcive();
		return contracts.stream().map(contractMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
	}
}
