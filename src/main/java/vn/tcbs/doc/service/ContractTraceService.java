package vn.tcbs.doc.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import vn.tcbs.doc.service.dto.ContractTraceDTO;

/**
 * Service Interface for managing ContractTrace.
 */
public interface ContractTraceService {

    /**
     * Save a contract_trace.
     *
     * @param contractTraceDTO the entity to save
     * @return the persisted entity
     */
    ContractTraceDTO save(ContractTraceDTO contractTraceDTO);

    /**
     *  Get all the contract_traces.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ContractTraceDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" contract_trace.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ContractTraceDTO findOne(Long id);

    /**
     *  Delete the "id" contract_trace.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
