package vn.tcbs.doc.service;

import java.util.List;

import vn.tcbs.doc.service.dto.ContractDetailsDTO;

/**
 * Service Interface for managing ContractDetails.
 */
public interface ContractDetailsService {

    /**
     * Save a contract_details.
     *
     * @param contractDetailsDTO the entity to save
     * @return the persisted entity
     */
    ContractDetailsDTO save(ContractDetailsDTO contractDetailsDTO);

    /**
     *  Get all the contract_details.
     *  
     *  @return the list of entities
     */
    List<ContractDetailsDTO> findAll(Long contractId);

    /**
     *  Get the "id" contract_details.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ContractDetailsDTO findOne(Long id);

    /**
     *  Delete the "id" contract_details.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
