package vn.tcbs.doc.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import vn.tcbs.doc.service.dto.ContractDetailUnsignDTO;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.service.dto.ContractTraceDTO;

public interface ContractPublicService {

	List<ContractDetailsDTO> findAll(long contractID);

	List<ContractDetailsDTO> findAll(int productID, Date applyDate);

	List<ContractDetailsDTO> findAll(String productCode, Date applyDate);

	List<ContractTraceDTO> findAllSigned(int orderID);

	List<ContractDetailsDTO> findAllUnsign(ContractDetailUnsignDTO contractDetailUnsignDTO);

	ContractTraceDTO sign(ContractTraceDTO contractTraceDTO);

}
