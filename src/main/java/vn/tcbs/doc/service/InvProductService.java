package vn.tcbs.doc.service;

import java.util.List;

import vn.tcbs.doc.domain.InvInvestmentProduct;

public interface InvProductService {
	
	public List<InvInvestmentProduct> findProduct();

}
