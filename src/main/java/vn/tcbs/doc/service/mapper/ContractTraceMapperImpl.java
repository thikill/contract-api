package vn.tcbs.doc.service.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import org.springframework.stereotype.Component;

import vn.tcbs.doc.domain.ContractTrace;
import vn.tcbs.doc.service.dto.ContractTraceDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-05-18T10:08:15+0700",
    comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_121 (Oracle Corporation)"
)
@Component
public class ContractTraceMapperImpl implements ContractTraceMapper {

    @Override
    public ContractTrace toEntity(ContractTraceDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ContractTrace contractTrace = new ContractTrace();

        contractTrace.setId( dto.getId() );
        contractTrace.setContract_id( dto.getContract_id() );
        contractTrace.setContract_detail_id( dto.getContract_detail_id() );
        contractTrace.setOrder_id( dto.getOrder_id() );
        contractTrace.setSign_time( dto.getSign_time() );
        contractTrace.setSign_by( dto.getSign_by() );
        contractTrace.setContent( dto.getContent() );

        return contractTrace;
    }

    @Override
    public ContractTraceDTO toDto(ContractTrace entity) {
        if ( entity == null ) {
            return null;
        }

        ContractTraceDTO contractTraceDTO = new ContractTraceDTO();

        contractTraceDTO.setId( entity.getId() );
        contractTraceDTO.setContract_id( entity.getContract_id() );
        contractTraceDTO.setContract_detail_id( entity.getContract_detail_id() );
        contractTraceDTO.setOrder_id( entity.getOrder_id() );
        contractTraceDTO.setSign_time( entity.getSign_time() );
        contractTraceDTO.setSign_by( entity.getSign_by() );
        contractTraceDTO.setContent( entity.getContent() );

        return contractTraceDTO;
    }

    @Override
    public List<ContractTrace> toEntity(List<ContractTraceDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ContractTrace> list = new ArrayList<ContractTrace>();
        for ( ContractTraceDTO contractTraceDTO : dtoList ) {
            list.add( toEntity( contractTraceDTO ) );
        }

        return list;
    }

    @Override
    public List<ContractTraceDTO> toDto(List<ContractTrace> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ContractTraceDTO> list = new ArrayList<ContractTraceDTO>();
        for ( ContractTrace contractTrace : entityList ) {
            list.add( toDto( contractTrace ) );
        }

        return list;
    }
}
