package vn.tcbs.doc.service.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2017-05-17T15:49:48+0700",
    comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_121 (Oracle Corporation)"
)
@Component
public class ContractDetailsMapperImpl implements ContractDetailsMapper {

    @Autowired
    private ContractMapper contractMapper;

    @Override
    public List<ContractDetails> toEntity(List<ContractDetailsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ContractDetails> list = new ArrayList<ContractDetails>();
        for ( ContractDetailsDTO contractDetailsDTO : dtoList ) {
            list.add( toEntity( contractDetailsDTO ) );
        }

        return list;
    }

    @Override
    public List<ContractDetailsDTO> toDto(List<ContractDetails> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ContractDetailsDTO> list = new ArrayList<ContractDetailsDTO>();
        for ( ContractDetails contractDetails : entityList ) {
            list.add( toDto( contractDetails ) );
        }

        return list;
    }

    @Override
    public ContractDetailsDTO toDto(ContractDetails contractDetails) {
        if ( contractDetails == null ) {
            return null;
        }

        ContractDetailsDTO contractDetailsDTO = new ContractDetailsDTO();

        contractDetailsDTO.setContractId( contract_detailsContractId( contractDetails ) );
        contractDetailsDTO.setId( contractDetails.getId() );
        contractDetailsDTO.setItem_name( contractDetails.getItem_name() );
        contractDetailsDTO.setContent( contractDetails.getContent() );
        contractDetailsDTO.setCreated_date( contractDetails.getCreated_date() );

        return contractDetailsDTO;
    }

    @Override
    public ContractDetails toEntity(ContractDetailsDTO contractDetailsDTO) {
        if ( contractDetailsDTO == null ) {
            return null;
        }

        ContractDetails contractDetails = new ContractDetails();

        contractDetails.setContract( contractMapper.fromId( contractDetailsDTO.getContractId() ) );
        contractDetails.setId( contractDetailsDTO.getId() );
        contractDetails.setItem_name( contractDetailsDTO.getItem_name() );
        contractDetails.setContent( contractDetailsDTO.getContent() );
        contractDetails.setCreated_date( contractDetailsDTO.getCreated_date() );

        return contractDetails;
    }

    private Long contract_detailsContractId(ContractDetails contractDetails) {

        if ( contractDetails == null ) {
            return null;
        }
        Contract contract = contractDetails.getContract();
        if ( contract == null ) {
            return null;
        }
        Long id = contract.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
