package vn.tcbs.doc.service.mapper;



import org.mapstruct.Mapper;

import vn.tcbs.doc.domain.ContractTrace;
import vn.tcbs.doc.service.dto.ContractTraceDTO;

/**
 * Mapper for the entity ContractTrace and its DTO ContractTraceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContractTraceMapper extends EntityMapper <ContractTraceDTO, ContractTrace> {
    
    
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default ContractTrace fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContractTrace contractTrace = new ContractTrace();
        contractTrace.setId(id);
        return contractTrace;
    }
}
