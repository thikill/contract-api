package vn.tcbs.doc.service.mapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import org.springframework.stereotype.Component;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.service.dto.ContractDTO;

@Generated(value = "org.mapstruct.ap.MappingProcessor", date = "2017-05-17T15:49:48+0700", comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_121 (Oracle Corporation)")
@Component
public class ContractMapperImpl implements ContractMapper {

	@Override
	public ContractDTO toDto(Contract entity) {
		if (entity == null) {
			return null;
		}

		ContractDTO contractDTO = new ContractDTO();

		contractDTO.setId(entity.getId());
		contractDTO.setContract_name(entity.getContract_name());
		contractDTO.setContract_code(entity.getContract_code());
		contractDTO.setApply_status(entity.getApply_status());
		contractDTO.setApply_time(entity.getApply_time());
		contractDTO.setVersion(entity.getVersion());
		contractDTO.setActive(entity.getActive());
		contractDTO.setProduct_id(entity.getProduct_id());
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String productName = entity.getInvInvestmentProduct() == null ? "" : entity.getInvInvestmentProduct().getProductName();
		contractDTO.setContract(entity.getContract_name() + "_"
				+ (entity.getApply_time() == null ? "" : df.format(entity.getApply_time())) + "_"
				+ productName);
		contractDTO.setProductName(productName);
		return contractDTO;
	}

	@Override
	public List<Contract> toEntity(List<ContractDTO> dtoList) {
		if (dtoList == null) {
			return null;
		}

		List<Contract> list = new ArrayList<Contract>();
		for (ContractDTO contractDTO : dtoList) {
			list.add(toEntity(contractDTO));
		}

		return list;
	}

	@Override
	public List<ContractDTO> toDto(List<Contract> entityList) {
		if (entityList == null) {
			return null;
		}

		List<ContractDTO> list = new ArrayList<ContractDTO>();
		for (Contract contract : entityList) {
			list.add(toDto(contract));
		}

		return list;
	}

	@Override
	public Contract toEntity(ContractDTO contractDTO) {
		if (contractDTO == null) {
			return null;
		}

		Contract contract = new Contract();

		contract.setId(contractDTO.getId());
		contract.setContract_name(contractDTO.getContract_name());
		contract.setContract_code(contractDTO.getContract_code());
		contract.setApply_status(contractDTO.getApply_status());
		contract.setApply_time(contractDTO.getApply_time());
		contract.setVersion(contractDTO.getVersion());
		contract.setActive(contractDTO.getActive());
		contract.setProduct_id(contractDTO.getProduct_id());

		return contract;
	}
}
