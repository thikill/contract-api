package vn.tcbs.doc.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;

/**
 * Mapper for the entity ContractDetails and its DTO ContractDetailsDTO.
 */
@Mapper(componentModel = "spring", uses = {ContractMapper.class, })
public interface ContractDetailsMapper extends EntityMapper <ContractDetailsDTO, ContractDetails> {
    @Mapping(source = "contract.id", target = "contractId")
    ContractDetailsDTO toDto(ContractDetails contractDetails); 
    @Mapping(source = "contractId", target = "contract")
    ContractDetails toEntity(ContractDetailsDTO contractDetailsDTO); 
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default ContractDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContractDetails contractDetails = new ContractDetails();
        contractDetails.setId(id);
        return contractDetails;
    }
}
