package vn.tcbs.doc.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.service.dto.ContractDTO;

/**
 * Mapper for the entity Contract and its DTO ContractDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContractMapper extends EntityMapper <ContractDTO, Contract> {
    
    @Mapping(target = "contract_details", ignore = true)
    Contract toEntity(ContractDTO contractDTO); 
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default Contract fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contract contract = new Contract();
        contract.setId(id);
        return contract;
    }
}
