package vn.tcbs.doc.service.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the ContractTrace entity.
 */
public class ContractTraceDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5082001026646665874L;

	private Long id;

    private Integer contract_id;

    private Integer contract_detail_id;

    private Integer order_id;

    private Date sign_time;

    private String sign_by;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getContract_id() {
        return contract_id;
    }

    public void setContract_id(Integer contract_id) {
        this.contract_id = contract_id;
    }

    public Integer getContract_detail_id() {
        return contract_detail_id;
    }

    public void setContract_detail_id(Integer contract_detail_id) {
        this.contract_detail_id = contract_detail_id;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public Date getSign_time() {
        return sign_time;
    }

    public void setSign_time(Date sign_time) {
        this.sign_time = sign_time;
    }

    public String getSign_by() {
        return sign_by;
    }

    public void setSign_by(String sign_by) {
        this.sign_by = sign_by;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContractTraceDTO contractTraceDTO = (ContractTraceDTO) o;
        if(contractTraceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractTraceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractTraceDTO{" +
            "id=" + getId() +
            ", contract_id='" + getContract_id() + "'" +
            ", contract_detail_id='" + getContract_detail_id() + "'" +
            ", order_id='" + getOrder_id() + "'" +
            ", sign_time='" + getSign_time() + "'" +
            ", sign_by='" + getSign_by() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
