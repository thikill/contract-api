package vn.tcbs.doc.service.dto;


import java.io.Serializable;
import java.util.Map;

/**
 * A DTO for the Contract entity.
 */
public class ContractDetailUnsignDTO implements Serializable {


	private static final long serialVersionUID = 763499660575404037L;
	
	private Integer orderID;
	private String tcbsID;
	private Map<String, Object> data;
	
	public Integer getOrderID() {
		return orderID;
	}
	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}
	public String getTcbsID() {
		return tcbsID;
	}
	public void setTcbsID(String tcbsID) {
		this.tcbsID = tcbsID;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
}
