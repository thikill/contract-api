package vn.tcbs.doc.service.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the ContractDetails entity.
 */
public class ContractDetailsDTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1391993596176701565L;

	private Long id;

    private String item_name;

    private String content;

    private Date created_date;

    private Long contractId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContractDetailsDTO contractDetailsDTO = (ContractDetailsDTO) o;
        if(contractDetailsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractDetailsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractDetailsDTO{" +
            "id=" + getId() +
            ", item_name='" + getItem_name() + "'" +
            ", content='" + getContent() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            "}";
    }
}
