package vn.tcbs.doc.service.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the Contract entity.
 */
public class ContractDTO implements Serializable {

	private static final long serialVersionUID = 1840684948544697582L;

	private Long id;

    private String contract_name;

    private String contract_code;

    private Integer apply_status;

    private Date apply_time;

    private String version;
    
    private Integer product_id;
    
    private String contract;
    
    public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	private String productName;
    
    private Integer copy_contract_id;

    public String getContract() {
		return contract;
	}

	public Integer getCopy_contract_id() {
		return copy_contract_id;
	}

	public void setCopy_contract_id(Integer copy_contract_id) {
		this.copy_contract_id = copy_contract_id;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	private Integer active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public String getContract_code() {
        return contract_code;
    }

    public void setContract_code(String contract_code) {
        this.contract_code = contract_code;
    }

    public Integer getApply_status() {
        return apply_status;
    }

    public void setApply_status(Integer apply_status) {
        this.apply_status = apply_status;
    }

    public Date getApply_time() {
        return apply_time;
    }

    public void setApply_time(Date apply_time) {
        this.apply_time = apply_time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContractDTO contractDTO = (ContractDTO) o;
        if(contractDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractDTO{" +
            "id=" + getId() +
            ", contract_name='" + getContract_name() + "'" +
            ", contract_code='" + getContract_code() + "'" +
            ", apply_status='" + getApply_status() + "'" +
            ", apply_time='" + getApply_time() + "'" +
            ", version='" + getVersion() + "'" +
            ", active='" + getActive() + "'" +
            "}";
    }
}
