package vn.tcbs.doc.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcbs.doc.service.ContractDetailsService;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.web.rest.util.HeaderUtil;
import vn.tcbs.doc.web.rest.util.ResponseUtil;

/**
 * REST controller for managing ContractDetails.
 */
@RestController
@RequestMapping("/api")
public class ContractDetailsResource {

    private final Logger log = LoggerFactory.getLogger(ContractDetailsResource.class);

    private static final String ENTITY_NAME = "contract_details";
        
    private final ContractDetailsService contractDetailsService;

    public ContractDetailsResource(ContractDetailsService contractDetailsService) {
        this.contractDetailsService = contractDetailsService;
    }

    /**
     * POST  /contract-details : Create a new contract_details.
     *
     * @param contractDetailsDTO the contract_detailsDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contract_detailsDTO, or with status 400 (Bad Request) if the contract_details has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contract-details")
    
    public ResponseEntity<ContractDetailsDTO> createContract_details(@RequestBody ContractDetailsDTO contractDetailsDTO) throws URISyntaxException {
        log.debug("REST request to save ContractDetails : {}", contractDetailsDTO);
        if (contractDetailsDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new contract_details cannot already have an ID")).body(null);
        }
        ContractDetailsDTO result = contractDetailsService.save(contractDetailsDTO);
        return ResponseEntity.created(new URI("/api/contract-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contract-details : Updates an existing contract_details.
     *
     * @param contractDetailsDTO the contract_detailsDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contract_detailsDTO,
     * or with status 400 (Bad Request) if the contract_detailsDTO is not valid,
     * or with status 500 (Internal Server Error) if the contract_detailsDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contract-details")
    
    public ResponseEntity<ContractDetailsDTO> updateContract_details(@RequestBody ContractDetailsDTO contractDetailsDTO) throws URISyntaxException {
        log.debug("REST request to update ContractDetails : {}", contractDetailsDTO);
        if (contractDetailsDTO.getId() == null) {
            return createContract_details(contractDetailsDTO);
        }
        ContractDetailsDTO result = contractDetailsService.save(contractDetailsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contractDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contract-details : get all the contract_details.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contract_details in body
     */
    @GetMapping("/contract-details/all/{contractId}")
    
    public List<ContractDetailsDTO> getAllContract_details(@PathVariable Long contractId) {
    	
        log.debug("REST request to get all ContractDetails" + contractId);
        return contractDetailsService.findAll(contractId);
    }

    /**
     * GET  /contract-details/:id : get the "id" contract_details.
     *
     * @param id the id of the contract_detailsDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contract_detailsDTO, or with status 404 (Not Found)
     */
    @GetMapping("/contract-details/{id}")
    
    public ResponseEntity<ContractDetailsDTO> getContract_details(@PathVariable Long id) {
        log.debug("REST request to get ContractDetails : {}", id);
        ContractDetailsDTO contractDetailsDTO = contractDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(contractDetailsDTO));
    }

    /**
     * DELETE  /contract-details/:id : delete the "id" contract_details.
     *
     * @param id the id of the contract_detailsDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contract-details/{id}")
    
    public ResponseEntity<Void> deleteContract_details(@PathVariable Long id) {
        log.debug("REST request to delete ContractDetails : {}", id);
        contractDetailsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
