package vn.tcbs.doc.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.FormParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.tcbs.doc.service.ContractPublicService;
import vn.tcbs.doc.service.dto.ContractDTO;
import vn.tcbs.doc.service.dto.ContractDetailUnsignDTO;
import vn.tcbs.doc.service.dto.ContractDetailsDTO;
import vn.tcbs.doc.service.dto.ContractTraceDTO;
import vn.tcbs.doc.web.rest.util.HeaderUtil;
import vn.tcbs.doc.web.rest.util.PaginationUtil;

/**
 * REST controller for managing ContractDetails.
 */
@RestController
@RequestMapping("/api")
public class ContractPublicResource {
	private static final String ENTITY_NAME = "contract_trace";
	private final Logger log = LoggerFactory.getLogger(ContractPublicResource.class);
	private final ContractPublicService contractPublicService;

	public ContractPublicResource(ContractPublicService contractPublicService) {
		this.contractPublicService = contractPublicService;
	}

	@GetMapping("/contract/{contractID}")
	public List<ContractDetailsDTO> findAll(@PathVariable long contractID) {
		log.debug("REST request to get a page of Contracts");
		List<ContractDetailsDTO> listContractDetailsDTO = contractPublicService.findAll(contractID);
		return listContractDetailsDTO;
	}

	@GetMapping("/contract/byid/{productID}/{applyDate}")
	public List<ContractDetailsDTO> findAll(@PathVariable int productID, @PathVariable long applyDate) {
		log.debug("REST request to get a page of Contracts");
		List<ContractDetailsDTO> listContractDetailsDTO = contractPublicService.findAll(productID, new Date(applyDate));
		return listContractDetailsDTO;
	}

	@GetMapping("/contract/bycode/{productCode}/{applyDate}")
	public List<ContractDetailsDTO> findAll(@PathVariable String productCode, @PathVariable long applyDate) {
		log.debug("REST request to get a page of Contracts");
		List<ContractDetailsDTO> listContractDetailsDTO = contractPublicService.findAll(productCode,
				new Date(applyDate));
		return listContractDetailsDTO;
	}

	@GetMapping("/contract-signed/{orderID}")
	public List<ContractTraceDTO> findAllSigned(@PathVariable int orderID) {
		log.debug("REST request to get a page of Contracts");
		List<ContractTraceDTO> listContractTraceDTO = contractPublicService.findAllSigned(orderID);
		return listContractTraceDTO;
	}

	@PostMapping("/unsign")
	public List<ContractDetailsDTO> findAllUnSigned(@RequestBody String jsonData) {
		log.debug("REST request to get a page of Contracts");
		ObjectMapper mapper = new ObjectMapper();
		ContractDetailUnsignDTO contractDetailUnsignDTO = null;
		try {
			contractDetailUnsignDTO = mapper.readValue(jsonData, ContractDetailUnsignDTO.class);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<ContractDetailsDTO> details = contractPublicService.findAllUnsign(contractDetailUnsignDTO);
		return details;
	}

	@PostMapping("/sign")
	public ResponseEntity<ContractTraceDTO> sign(@RequestBody ContractTraceDTO contractTraceDTO)
			throws URISyntaxException {
		log.debug("REST request to save ContractTrace : {}", contractTraceDTO);
		if (contractTraceDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new contract_trace cannot already have an ID")).body(null);
		}
		ContractTraceDTO result = contractPublicService.sign(contractTraceDTO);
		return ResponseEntity.created(new URI("/api/contract-traces/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.set(2018, 4, 1);
		System.out.println(cal.getTimeInMillis());
	}
}
