package vn.tcbs.doc.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcbs.doc.service.ContractTraceService;
import vn.tcbs.doc.service.dto.ContractTraceDTO;
import vn.tcbs.doc.web.rest.util.HeaderUtil;
import vn.tcbs.doc.web.rest.util.PaginationUtil;
import vn.tcbs.doc.web.rest.util.ResponseUtil;

/**
 * REST controller for managing ContractTrace.
 */
@RestController
@RequestMapping("/api")
public class ContractTraceResource {

    private final Logger log = LoggerFactory.getLogger(ContractTraceResource.class);

    private static final String ENTITY_NAME = "contract_trace";
        
    private final ContractTraceService contractTraceService;

    public ContractTraceResource(ContractTraceService contractTraceService) {
        this.contractTraceService = contractTraceService;
    }

    /**
     * POST  /contract-traces : Create a new contract_trace.
     *
     * @param contractTraceDTO the contract_traceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contract_traceDTO, or with status 400 (Bad Request) if the contract_trace has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contract-traces")
    public ResponseEntity<ContractTraceDTO> createContract_trace(@RequestBody ContractTraceDTO contractTraceDTO) throws URISyntaxException {
        log.debug("REST request to save ContractTrace : {}", contractTraceDTO);
        if (contractTraceDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new contract_trace cannot already have an ID")).body(null);
        }
        ContractTraceDTO result = contractTraceService.save(contractTraceDTO);
        return ResponseEntity.created(new URI("/api/contract-traces/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contract-traces : Updates an existing contract_trace.
     *
     * @param contractTraceDTO the contract_traceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contract_traceDTO,
     * or with status 400 (Bad Request) if the contract_traceDTO is not valid,
     * or with status 500 (Internal Server Error) if the contract_traceDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contract-traces")
    
    public ResponseEntity<ContractTraceDTO> updateContract_trace(@RequestBody ContractTraceDTO contractTraceDTO) throws URISyntaxException {
        log.debug("REST request to update ContractTrace : {}", contractTraceDTO);
        if (contractTraceDTO.getId() == null) {
            return createContract_trace(contractTraceDTO);
        }
        ContractTraceDTO result = contractTraceService.save(contractTraceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contractTraceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contract-traces : get all the contract_traces.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of contract_traces in body
     */
    @GetMapping("/contract-traces")
    
    public ResponseEntity<List<ContractTraceDTO>> getAllContract_traces( Pageable pageable) {
        log.debug("REST request to get a page of Contract_traces");
        Page<ContractTraceDTO> page = contractTraceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contract-traces");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /contract-traces/:id : get the "id" contract_trace.
     *
     * @param id the id of the contract_traceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contract_traceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/contract-traces/{id}")
    
    public ResponseEntity<ContractTraceDTO> getContract_trace(@PathVariable Long id) {
        log.debug("REST request to get ContractTrace : {}", id);
        ContractTraceDTO contractTraceDTO = contractTraceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(contractTraceDTO));
    }

    /**
     * DELETE  /contract-traces/:id : delete the "id" contract_trace.
     *
     * @param id the id of the contract_traceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contract-traces/{id}")
    
    public ResponseEntity<Void> deleteContract_trace(@PathVariable Long id) {
        log.debug("REST request to delete ContractTrace : {}", id);
        contractTraceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
