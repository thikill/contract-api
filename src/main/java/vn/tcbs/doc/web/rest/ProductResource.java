package vn.tcbs.doc.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcbs.doc.domain.InvInvestmentProduct;
import vn.tcbs.doc.service.InvProductService;

/**
 * REST controller for managing ContractDetails.
 */
@RestController
@RequestMapping("/api")
public class ProductResource {

	private final Logger log = LoggerFactory.getLogger(ProductResource.class);

	private final InvProductService invProductService;

	public ProductResource(InvProductService invProductService) {
		this.invProductService = invProductService;
	}

	/**
	 * GET /contract-details : get all the contract_details.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         contract_details in body
	 */
	@GetMapping("/products")
	public List<InvInvestmentProduct> getProducts() {
		log.debug("REST request to getProducts");
		return invProductService.findProduct();
	}

}
