package vn.tcbs.doc.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import vn.tcbs.doc.domain.ContractTrace;

/**
 * Spring Data JPA repository for the ContractTrace entity.
 */
public interface ContractTraceRepository extends JpaRepository<ContractTrace,Long> {

}
