package vn.tcbs.doc.repository;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository
public class VelocityRepository {
	private final Logger logger = LoggerFactory.getLogger(VelocityRepository.class);

	public VelocityEngine getVelocityEngine() {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();
		return ve;
	}

	public String getVelocityContextData(Template template, VelocityContext velocityContext) {
		try {
			StringWriter stringWirter = new StringWriter();
			template.merge(velocityContext, stringWirter);
			return stringWirter.toString();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	public String mergeObjToTemplate(String templatePath, Object objInfo) {
		String content = "";
		try {
			Template template = getVelocityEngine().getTemplate(templatePath, "UTF-8");
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("numberTool", new NumberTool());
			velocityContext.put("date", new DateTool());
			velocityContext.put("objInfo", objInfo);
			content = getVelocityContextData(template, velocityContext);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return content;
	}
	
	
	public String mergeMapToString(String templateContent, Map<String, Object> model) {
		String content = "";
		try {
			RuntimeServices runtimeServices = RuntimeSingleton.getRuntimeServices();
			StringReader reader = new StringReader(templateContent);
			SimpleNode node = runtimeServices.parse(reader, "Template name");
			Template template = new Template();
			template.setRuntimeServices(runtimeServices);
			template.setData(node);
			template.initDocument();
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("numberTool", new NumberTool());
			velocityContext.put("date", new DateTool());
			for (Entry<String, Object> entry : model.entrySet()) {
				velocityContext.put(entry.getKey(), entry.getValue());
			}
			content = getVelocityContextData(template, velocityContext);			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return content;
	}
	

}
