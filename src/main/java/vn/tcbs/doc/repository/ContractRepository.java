package vn.tcbs.doc.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.SqlResultSetMapping;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import vn.tcbs.doc.domain.Contract;
import vn.tcbs.doc.domain.ContractDetails;
import vn.tcbs.doc.domain.ContractTrace;

/**
 * Spring Data JPA repository for the Contract entity.
 */
public interface ContractRepository extends JpaRepository<Contract, Long> {

	@Query(value = "SELECT top 1 * FROM INV_CONTRACT a WHERE a.product_id = :productID AND  a.apply_status = 1 and a.active=1 and  a.apply_time <= :applyDate  order by a.apply_time desc", nativeQuery = true)
	public Contract findOne(@Param("productID") int productID, @Param("applyDate") Date applyDate);

	@Query(value = "SELECT top 1 a.* FROM INV_CONTRACT a, INV_INVESTMENT_PRODUCT  b WHERE b.product_code = :productCode AND  a.apply_status = 1 and a.active=1 and  a.apply_time <= :applyDate  order by a.apply_time desc", nativeQuery = true)
	public Contract findOne(@Param("productCode") String productCode, @Param("applyDate") Date applyDate);

	@Query("select b from ContractDetails a, ContractTrace b where a.id = b.contract_detail_id and b.order_id = :orderID")
	public List<ContractTrace> findAllSigned(@Param("orderID") int orderID);

	@Query(value = "select top 1  b.CONTRACT_ID from INV_CONTRACT_TRACE a,  INV_CONTRACT_DETAILS b where a.CONTRACT_DETAIL_ID = b.ID and ORDER_ID= :orderID", nativeQuery = true)
	public int getContractIDByDetailID(@Param("orderID") int orderID);
	
	


	//@Query(value = "select a.* from INV_CONTRACT_DETAILS a where a.CONTRACT_ID=:contractID and a.ID not in (select  a.CONTRACT_DETAIL_ID from INV_CONTRACT_TRACE a where  ORDER_ID=:orderID)", nativeQuery = true)

	// @Query("select a from ContractDetails a where a.")
	//public List<ContractDetails> findAllUnSigned(@Param("orderID") int orderID, @Param("contractID") int contractID);

	@Query("SELECT count(*)  FROM Contract a WHERE active = 1")
	public int countActive();

	@Query("SELECT a FROM Contract a WHERE active = 1 order by a.apply_status desc, a.updated_time desc")
	public Page<Contract> findByAcive(Pageable pageRequest);

	@Query("SELECT a FROM Contract a WHERE active = 1 order by a.contract_name asc, a.apply_status desc, a.updated_time desc")
	public List<Contract> findByAcive();

	// Select top 1 * from Contract a Where a.applyDate >= :applyDate Order by
	// a.ApplyDate Desc;
}
