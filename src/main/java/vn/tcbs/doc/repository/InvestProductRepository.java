package vn.tcbs.doc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import vn.tcbs.doc.domain.InvInvestmentProduct;

public interface InvestProductRepository extends  JpaRepository<InvInvestmentProduct, Integer>{
	@Query("SELECT a FROM InvInvestmentProduct a, InvUnderlyingInstrument b   where a.underlyingInstrumentId = b.underlyingId  and b.instrumentType in ('BOND', 'FUND')")
	public List<InvInvestmentProduct> findProduct();

}
