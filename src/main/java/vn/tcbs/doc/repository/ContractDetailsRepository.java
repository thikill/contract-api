package vn.tcbs.doc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.tcbs.doc.domain.ContractDetails;

/**
 * Spring Data JPA repository for the ContractDetails entity.
 */
public interface ContractDetailsRepository extends JpaRepository<ContractDetails, Long> {
}
