package vn.tcbs.doc.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A Contract.
 */
@Entity
@Table(name = "INV_CONTRACT")
public class Contract implements Serializable {

	private static final long serialVersionUID = 347200890940060854L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contract_name")
    private String contract_name;

    @Column(name = "contract_code")
    private String contract_code;

    @Column(name = "apply_status")
    private Integer apply_status;

    @Column(name = "apply_time")
    private Date apply_time;

    @Column(name = "version")
    private String version;

    @Column(name = "active")
    private Integer active;
    
    
    @Column(name = "product_id")
    private Integer product_id;
    
    @Column(name = "created_time", updatable= false)
    private Date created_time;
    @Column(name = "updated_time")
    private Date updated_time;
    @Column(name = "approved_time")
    private Date approved_time;
    
    public Date getCreated_time() {
		return created_time;
	}

	public void setCreated_time(Date created_time) {
		this.created_time = created_time;
	}

	public Date getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(Date updated_time) {
		this.updated_time = updated_time;
	}

	public Date getApproved_time() {
		return approved_time;
	}

	public void setApproved_time(Date approved_time) {
		this.approved_time = approved_time;
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public InvInvestmentProduct getInvInvestmentProduct() {
		return invInvestmentProduct;
	}

	public void setInvInvestmentProduct(InvInvestmentProduct invInvestmentProduct) {
		this.invInvestmentProduct = invInvestmentProduct;
	}

	@ManyToOne
	@JoinColumn(name="product_id", insertable=false, updatable=false)
    private InvInvestmentProduct invInvestmentProduct;
	
	@OneToMany(mappedBy = "contract")
    @JsonIgnore
    private Set<ContractDetails> contractDetails = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContract_name() {
        return contract_name;
    }

    public Contract contract_name(String contract_name) {
        this.contract_name = contract_name;
        return this;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public String getContract_code() {
        return contract_code;
    }

    public Contract contract_code(String contract_code) {
        this.contract_code = contract_code;
        return this;
    }

    public void setContract_code(String contract_code) {
        this.contract_code = contract_code;
    }

    public Integer getApply_status() {
        return apply_status;
    }

    public Contract apply_status(Integer apply_status) {
        this.apply_status = apply_status;
        return this;
    }

    public void setApply_status(Integer apply_status) {
        this.apply_status = apply_status;
    }

    public Date getApply_time() {
        return apply_time;
    }

    public Contract apply_time(Date apply_time) {
        this.apply_time = apply_time;
        return this;
    }

    public void setApply_time(Date apply_time) {
        this.apply_time = apply_time;
    }

    public String getVersion() {
        return version;
    }

    public Contract version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getActive() {
        return active;
    }

    public Contract active(Integer active) {
        this.active = active;
        return this;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Set<ContractDetails> getContract_details() {
        return contractDetails;
    }

    public Contract contractDetails(Set<ContractDetails> contractDetails) {
        this.contractDetails = contractDetails;
        return this;
    }

    public Contract addContract_details(ContractDetails contractDetails) {
        this.contractDetails.add(contractDetails);
        contractDetails.setContract(this);
        return this;
    }

    public Contract removeContract_details(ContractDetails contractDetails) {
        this.contractDetails.remove(contractDetails);
        contractDetails.setContract(null);
        return this;
    }

    public void setContract_details(Set<ContractDetails> contractDetails) {
        this.contractDetails = contractDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contract contract = (Contract) o;
        if (contract.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contract.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Contract{" +
            "id=" + getId() +
            ", contract_name='" + getContract_name() + "'" +
            ", contract_code='" + getContract_code() + "'" +
            ", apply_status='" + getApply_status() + "'" +
            ", apply_time='" + getApply_time() + "'" +
            ", version='" + getVersion() + "'" +
            ", active='" + getActive() + "'" +
            "}";
    }
}
