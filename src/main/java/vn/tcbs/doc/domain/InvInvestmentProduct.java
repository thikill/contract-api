package vn.tcbs.doc.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the INV_INVESTMENT_PRODUCT database table.
 * 
 */
@Entity
@Table(name = "INV_INVESTMENT_PRODUCT")
@NamedQuery(name = "InvInvestmentProduct.findAll", query = "SELECT i FROM InvInvestmentProduct i")
public class InvInvestmentProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PRODUCT_ID")
	private Integer productId;

	@Column(name = "CONTRACT_CODE")
	private String contractCode;

	@Column(name = "PRODUCT_CODE")
	private String productCode;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "UNDERLYING_INSTRUMENT_ID")
	private Integer underlyingInstrumentId;

	@OneToMany(mappedBy = "invInvestmentProduct")
	@JsonIgnore
	private Set<Contract> products = new HashSet<>();

	public Set<Contract> getProducts() {
		return products;
	}

	public void setProducts(Set<Contract> products) {
		this.products = products;
	}

	public InvInvestmentProduct() {
	}

	public Integer getProductId() {
		return this.productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getUnderlyingInstrumentId() {
		return this.underlyingInstrumentId;
	}

	public void setUnderlyingInstrumentId(Integer underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}

}