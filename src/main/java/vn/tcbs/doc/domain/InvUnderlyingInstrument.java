package vn.tcbs.doc.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the INV_UNDERLYING_INSTRUMENT database table.
 * 
 */
@Entity
@Table(name="INV_UNDERLYING_INSTRUMENT")
@NamedQuery(name="InvUnderlyingInstrument.findAll", query="SELECT i FROM InvUnderlyingInstrument i")
public class InvUnderlyingInstrument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UNDERLYING_ID")
	private Integer underlyingId;

	@Column(name="EXCHANGE")
	private String exchange;

	@Column(name="INSTRUMENT_ID")
	private Integer instrumentId;

	@Column(name="INSTRUMENT_TYPE")
	private String instrumentType;

	public InvUnderlyingInstrument() {
	}

	public Integer getUnderlyingId() {
		return this.underlyingId;
	}

	public void setUnderlyingId(Integer underlyingId) {
		this.underlyingId = underlyingId;
	}

	public String getExchange() {
		return this.exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public Integer getInstrumentId() {
		return this.instrumentId;
	}

	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}

	public String getInstrumentType() {
		return this.instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

}