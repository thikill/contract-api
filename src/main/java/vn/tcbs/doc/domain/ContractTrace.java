package vn.tcbs.doc.domain;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A ContractTrace.
 */
@Entity
@Table(name = "INV_CONTRACT_TRACE")
public class ContractTrace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "contract_id")
    private Integer contract_id;

    @Column(name = "contract_detail_id")
    private Integer contract_detail_id;

    @Column(name = "order_id")
    private Integer order_id;

    @Column(name = "sign_time")
    private Date sign_time;

    @Column(name = "sign_by")
    private String sign_by;

    @Column(name = "content")
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getContract_id() {
        return contract_id;
    }

    public ContractTrace contract_id(Integer contract_id) {
        this.contract_id = contract_id;
        return this;
    }

    public void setContract_id(Integer contract_id) {
        this.contract_id = contract_id;
    }

    public Integer getContract_detail_id() {
        return contract_detail_id;
    }

    public ContractTrace contract_detail_id(Integer contract_detail_id) {
        this.contract_detail_id = contract_detail_id;
        return this;
    }

    public void setContract_detail_id(Integer contract_detail_id) {
        this.contract_detail_id = contract_detail_id;
    }

    public Integer getOrder_id() {
        return order_id;
    }

    public ContractTrace order_id(Integer order_id) {
        this.order_id = order_id;
        return this;
    }

    public void setOrder_id(Integer order_id) {
        this.order_id = order_id;
    }

    public Date getSign_time() {
        return sign_time;
    }

    public ContractTrace sign_time(Date sign_time) {
        this.sign_time = sign_time;
        return this;
    }

    public void setSign_time(Date sign_time) {
        this.sign_time = sign_time;
    }

    public String getSign_by() {
        return sign_by;
    }

    public ContractTrace sign_by(String sign_by) {
        this.sign_by = sign_by;
        return this;
    }

    public void setSign_by(String sign_by) {
        this.sign_by = sign_by;
    }

    public String getContent() {
        return content;
    }

    public ContractTrace content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContractTrace contractTrace = (ContractTrace) o;
        if (contractTrace.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractTrace.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractTrace{" +
            "id=" + getId() +
            ", contract_id='" + getContract_id() + "'" +
            ", contract_detail_id='" + getContract_detail_id() + "'" +
            ", order_id='" + getOrder_id() + "'" +
            ", sign_time='" + getSign_time() + "'" +
            ", sign_by='" + getSign_by() + "'" +
            ", content='" + getContent() + "'" +
            "}";
    }
}
