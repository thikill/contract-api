package vn.tcbs.doc.domain;


import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * A ContractDetails.
 */
@Entity
@Table(name = "INV_CONTRACT_DETAILS")
public class ContractDetails implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "item_name")
    private String item_name;

    @Column(name = "content")
    private String content;

    @Column(name = "created_time", updatable= false)
    private Date created_date;
    
    @Column(name = "updated_time")
    private Date updated_time;

    public Date getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(Date updated_time) {
		this.updated_time = updated_time;
	}

	@ManyToOne
    private Contract contract;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItem_name() {
        return item_name;
    }

    public ContractDetails item_name(String item_name) {
        this.item_name = item_name;
        return this;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getContent() {
        return content;
    }

    public ContractDetails content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public ContractDetails created_date(Date created_date) {
        this.created_date = created_date;
        return this;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Contract getContract() {
        return contract;
    }

    public ContractDetails contract(Contract contract) {
        this.contract = contract;
        return this;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContractDetails contractDetails = (ContractDetails) o;
        if (contractDetails.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContractDetails{" +
            "id=" + getId() +
            ", item_name='" + getItem_name() + "'" +
            ", content='" + getContent() + "'" +
            ", created_date='" + getCreated_date() + "'" +
            "}";
    }
}
